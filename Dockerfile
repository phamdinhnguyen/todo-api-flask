FROM python:3.10.0a3-alpine3.12
COPY . /app
WORKDIR /app
RUN  pip install -r requirements.txt
ENTRYPOINT ['python]
CMD ['app.py']
